<?php

use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\ProjectInvestorController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
//Route::get('/', function () {
//    return view('welcome');
//});


Route::get('/login',  [LoginController::class, 'login_page'])->name('login');
Route::post('/login', [LoginController::class, 'login'])->name('login');


Route::middleware(['auth'])->group(function () {
    Route::get('/', function () {
        return redirect('/projects');
    });
    Route::get('/projects', [ProjectController::class, 'get_all_projects']);
    Route::get('/create-investment/{projectId}', [ProjectController::class, 'get_create_investment_page']);
    Route::post('/create-investment/{projectId}', [ProjectController::class, 'create_investment']);
    Route::get('/logout', [LoginController::class, 'logout']);
});
