<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Projects</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">
    </head>
    <body>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th scope="col">Finishes</th>
            <th scope="col">Requested Fund</th>
            <th scope="col">Owner</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($projects as $p)
            <tr>
                <td>{{$p->idProject}}</td>
                <td>{{$p->projectName}}</td>
                <td>{{$p->projectDescription}}</td>
                <td>{{$p->projectEndDate}}</td>
                <td>{{$p->requestedFund}}</td>
                <td>
                    @foreach($users as $u)
                        @if($u->idUser===$p->idUser)
                            {{$u->firstname.' '.$u->lastname}}
                        @endif
                    @endforeach
                </td>
                <td>

                    <span>
                    @if(\Illuminate\Support\Facades\Auth::user()->idUser!==$p->idUser)
                            <a href="/create-investment/{{$p->idProject}}"><button class="btn btn-primary" type="button">Invest</button></a>
                    @else
                            <a href="/investments/{{$p->idProject}}"><button class="btn btn-success" type="button">View Investments</button></a>
                        @endif
                    </span>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    </body>
</html>
