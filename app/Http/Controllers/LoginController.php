<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller
{
    function login_page() {
        Log::info("1111WOOOOOOW IAM DUCKING WOOOORKING");
        return view('login');
    }
    function login(Request $request) {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        Log::info(['email'=>$credentials['email'], 'password'=>$credentials['password']]);
        $q = Auth::attempt($credentials);
        Log::info("RES ".$q);
        if ($q) {
            $request->session()->regenerate();

            return redirect()->intended('/projects');
        }

        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ]);
    }

    function logout(Request $request) {
        Auth::logout();
        return redirect('login');
    }
}
