<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\ProjectInvestor;
use Illuminate\Support\Facades\Log;

class ProjectController extends Controller
{
    function get_all_projects() {
        return view('projects', ['projects'=>Project::all(), 'users'=>User::all()]);
    }

    function get_create_investment_page(int $projectId) {
        if(ProjectInvestor::where('idProject','=', $projectId)->where('idUser', '=',auth()->user()->idUser)->first()) {
            return view('already_invested_error');
        }
        $project = Project::where('idProject', '=', $projectId)->first();
        $totalInvestedSum = DB::table('projects_investors')->where('idProject', '=', $projectId)->sum('investmentFund');

        return view('create_investment', ['project'=>$project, 'totalInvestedSum'=>$totalInvestedSum]);
    }

    function create_investment(int $projectId, Request $request) {
        $fields = $request->validate(
            [
                'fund'=>'required|int',
                'date'=>'required|date'
            ]
        );

        $project = Project::where('idProject', '=', $projectId)->first();
        $totalInvestedSum = DB::table('projects_investors')->where('idProject', '=', $projectId)->sum('investmentFund');
        $remaining = $project->requestedFund-$totalInvestedSum;
        if($fields['fund']>$remaining) {
            throw new \Exception('Fund cannot be more than remaining : '.$remaining);
        }
        $date = Carbon::createFromFormat('Y-m-d', $fields['date']);
        $projectEndDate = Carbon::createFromFormat('Y-m-d', $project->projectEndDate);
        if($projectEndDate->lessThan($date)) {
            throw new \Exception('Maximum possible date is : '.$projectEndDate);
        }

        $projectInvestor = ProjectInvestor::create(
            [
                'idProject'=>$projectId,
                'idUser'=>auth()->user()->idUser,
                'investmentFund'=>$fields['fund'],
                'investmentDate'=>$fields['date']
            ]
        );
        return redirect('/projects');
    }
}
